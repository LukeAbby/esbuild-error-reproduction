// @ts-check

const esbuild = require("esbuild");

esbuild.build({
    entryPoints: [`./src/index.js`],
    outdir: "./dist",
    outbase: "./src",
    bundle: true,
    sourcemap: true,
    minify: true,
    incremental: false,
    define: {
        BUILD_REPLACES_VERBOSE: (
            process.env.BETTER_ROLLS_5E_VERBOSE === "true"
        ).toString(),
    },
    watch: false,
    plugins: [],
});
